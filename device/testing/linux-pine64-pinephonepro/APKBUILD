# Maintainer: Martijn Braam <martijn@brixit.nl>

pkgname=linux-pine64-pinephonepro
pkgver=5.16.3
pkgrel=0
pkgdesc="Mainline kernel for the pinephone pro"
arch="aarch64"
_flavor="${pkgname#linux-}"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-zram
	"
makedepends="
	bison
	findutils
	flex
	gmp-dev
	gzip
	linux-headers
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	postmarketos-installkernel
	rsync
	xz
	"

case "$CARCH" in
	aarch64*) _carch="arm64" ;;
	arm*) _carch="arm" ;;
esac

# Source
_config="config-$_flavor.$CARCH"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
_commit="138b2bb2032e6df014efe88e4a4bbdd4ad93b18d"
source="
	https://gitlab.com/pine64-org/linux/-/archive/ppp-$pkgver/linux-ppp-$pkgver.tar.gz
	config-$_flavor.aarch64
	"
builddir="$srcdir/linux-ppp-$pkgver"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))"

}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs-$_flavor
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}


sha512sums="
036dbe87699fce7b7f7d9ef519b4fccbc20e2fa73f372a43cf90066dd14121b51bdbc0b774256067310745d79572f1305d085615ea7cb882111dff5ac34aa554  linux-ppp-5.16.3.tar.gz
1fc43a1bca4cb2a82617568cee24d49acc172ddc8fcc2d652760c368b242e536629b6e0fda5f38b990ee2e5fb029c1592ea5bdd0f89a83bd3484bc8d533d64cc  config-pine64-pinephonepro.aarch64
"
